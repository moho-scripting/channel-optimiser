-- **************************************************
-- Provide Moho with the name of this script object
-- **************************************************

ScriptName = "bis_test_curve_opt"

-- **************************************************
-- General information about this script
-- **************************************************

bis_test_curve_opt = {}

function bis_test_curve_opt:Name()
	return 'Channel optimizer'
end

function bis_test_curve_opt:Version()
	return '1.0'
end

function bis_test_curve_opt:UILabel()
	return 'Channel optimizer'
end

function bis_test_curve_opt:Creator()
	return 'bis'
end

function bis_test_curve_opt:Description()
	return 'Channel optimizer, test version'
end

function bis_test_curve_opt:ColorizeIcon()
	return true
end

-- **************************************************
-- Is Relevant / Is Enabled
-- **************************************************

function bis_test_curve_opt:IsRelevant(moho)
	return true
end

function bis_test_curve_opt:IsEnabled(moho)
	return true
end



-- **************************************************
-- Prefs
-- **************************************************

function bis_test_curve_opt:LoadPrefs(prefs)
    self.Y_factor = prefs:GetFloat("bis_test_curve_opt.Y_factor", 0.01)
end

function bis_test_curve_opt:SavePrefs(prefs)
    prefs:SetFloat("bis_test_curve_opt.Y_factor", self.Y_factor)
end

function bis_test_curve_opt:ResetPrefs()
    self.Y_factor = 0.01
end

-- **************************************************
-- Recurring Values
-- **************************************************

bis_test_curve_opt.Y_factor = 0.01

-- **************************************************
-- Keyboard/Mouse Control
-- **************************************************

function bis_test_curve_opt:OnMouseDown(moho, mouseEvent)
	
end

function bis_test_curve_opt:OnMouseMoved(moho, mouseEvent)
	
end

function bis_test_curve_opt:OnMouseUp(moho, mouseEvent)
	
end

function bis_test_curve_opt:OnKeyDown(moho, keyEvent)
	
end

function bis_test_curve_opt:OnKeyUp(moho, keyEvent)
	
end

-- **************************************************
-- Tool Panel Layout
-- **************************************************

bis_test_curve_opt.Create_curve = MOHO.MSG_BASE
bis_test_curve_opt.Simplify_curve = MOHO.MSG_BASE + 1
bis_test_curve_opt.Apply_Curve = MOHO.MSG_BASE + 2
bis_test_curve_opt.Y_factor1 = MOHO.MSG_BASE + 3
bis_test_curve_opt.Chnl = nil
bis_test_curve_opt.Frames = {}
bis_test_curve_opt.Angles = {}
bis_test_curve_opt.Shifts = {}
bis_test_curve_opt.Y_factor = 0.01


function bis_test_curve_opt:DoLayout(moho, layout)
	-- Нам не нужен метод Localize. Мы не собираемся переводить этот интерфейс ни на китайский, ни на болгарский.
	
	self.Createcurve = LM.GUI.Button('Create curve', self.Create_curve)
	layout:AddChild(self.Createcurve, LM.GUI.ALIGN_LEFT, 0)

	self.Simplifycurve = LM.GUI.Button('Simplify curve', self.Simplify_curve)
	layout:AddChild(self.Simplifycurve, LM.GUI.ALIGN_LEFT, 0)

	self.ApplyCurve = LM.GUI.Button('Apply Curve', self.Apply_Curve)
	layout:AddChild(self.ApplyCurve, LM.GUI.ALIGN_LEFT, 0)
	
	self.Y_factorInput = LM.GUI.TextControl(0, "00.0000", self.Y_factor1, LM.GUI.FIELD_FLOAT, "Y factor")
    layout:AddChild(self.Y_factorInput, LM.GUI.ALIGN_CENTER, 0)
    self.Y_factorInput:SetToolTip("Y factor")

end

function bis_test_curve_opt:UpdateWidgets(moho)
	self.Y_factorInput:SetValue(self.Y_factor)
end

function bis_test_curve_opt:HandleMessage(moho, view, msg)
	if msg == self.Create_curve then
	print('Message Create_curve received')
	self:Createcurve_test(moho)
	elseif msg == self.Simplify_curve then
		print('Message Simplify_curve received')
	self:Simplify_curve_test(moho)	
	elseif msg == self.Apply_Curve then
		self:Applycurve_test(moho)
		print('Message Apply_Curve received')
	elseif msg == self.Y_factor then
        print('Message Y_factor received')
	elseif (msg == self.Y_factor1) then
		self.Y_factor = self.Y_factorInput:FloatValue()
		if (self.Y_factor < 0.001) then
			self.Y_factor = 0.001
		end
	else
		
	end
end

function bis_test_curve_opt:Applycurve_test(moho)
			local curve = moho:Mesh():Curve(0)
			local interp = MOHO.InterpSetting:new_local()
			--local channel=layer1:Bone(0).fAnimAngle
			--local layer1=moho:LayerAsBone(layer):Skeleton() --трактовать слой, как BoneLayer
			k=0
			for k = 0, moho:Mesh():Curve(0):CountPoints() do
					local Curvature=moho:Mesh():Curve(0):GetCurvature(k,  0)
					local interp = MOHO.InterpSetting:new_local()--создать объект класса InterpSetting
				   -- channel:GetKeyInterp(bis_test_curve_opt.Frames[i], interp)--отдать его в качестве параметра методу, который заполнит этот параметр значениями
					--moho:Mesh():Curve(0):GetWeight(k, 0, false)
					--moho:Mesh():Curve(0):GetWeight(k, 0, true)
					print("Curvature", tostring(Curvature))
					print("out  ", tostring(moho:Mesh():Curve(0):GetWeight(k, 0, false)), "in  ", tostring(moho:Mesh():Curve(0):GetWeight(k, 0, true)))

					
				end
				end

function bis_test_curve_opt:Simplify_curve_test(moho)
print(" CU_f ",tostring(moho:Mesh():Curve(0):GetOffset(0,  0, false)), " CU_t ",tostring(moho:Mesh():Curve(0):GetOffset(0,  0, true)))
print(" CU_f ",tostring(moho:Mesh():Curve(0):GetOffset(1,  0, false)), " CU_t ",tostring(moho:Mesh():Curve(0):GetOffset(1,  0, true)))			

	local mesh = moho:DrawingMesh()
	local didPrepUndo = false
	local epsilon = MOHO.SimplifyEpsilon(self.simplifyValue)
	for i = 0, mesh:CountCurves() - 1 do
		local curve = mesh:Curve(i)
		print("11", i)
		if (curve:IsSelected() or curve:IsPartiallySelected()) then
			if (not didPrepUndo) then
				moho.document:PrepUndo(moho.drawingLayer)
				moho.document:SetDirty()
				didPrepUndo = true
			end
			mesh:AdvancedCurveSimplification(i, moho.drawingLayerFrame, 0.0, epsilon, 0.01) -- 0.001, 0.0001, 0.01
		end
	end
	k=0
	for i = 0, bis_test_curve_opt.Chnl:Duration() do
		print("SetOffset true  угол вх", tostring(moho:Mesh():Curve(0):GetOffset(k,  0, true)))
		print("SetOffset true  угол исх", tostring(moho:Mesh():Curve(0):GetOffset(k,  0, false)))
		print("SetWeight true  длина вх", tostring(moho:Mesh():Curve(0):GetWeight(k,  0, true)))
		print("SetWeight false  длина исх", tostring(moho:Mesh():Curve(0):GetWeight(k,  0, false)))
	end
end

function bis_test_curve_opt:Createcurve_test(moho)
	-- **********************************************
	-- Наша конечная цель -- написать методы, обрабатывающие каждую из кривых. 
	-- В тестовом скрипте мы будем обрабатывать только одну захардкоденную заранее известную кривую. 
	-- Но в остальном метод должен быть максимально похож на тот, который мы будем применять в будущем для всех кривых по очереди
	-- По этому в начале мы должны задать все исходные данные 
	-- ( которые в будущем методе будут поставляться в виде аргументов )
	-- а дальше последует обработка этих данных
	-- **********************************************

	local channel = moho.layer:ControllingSkeleton():Bone(0).fAnimAngle -- канал анимации, с которым будем работать. В будущем будет аргументом метода.
	local mesh = moho:DrawingMesh() -- сетка, в которой будем рисовать кривую. В будущем заменится на LM_MeshPreview 
	local Yfactor = self.Y_factor -- значение этого свойства постоянно обновляется методом HandleMessage, нет никакой необходимости повторять этот код. 
	-- В будущем значение Yfactor будет каким-то образом вычисляться или захардкодено.
	local Xfactor = 1.33/channel:Duration() -- а вот коэффициент по X захардкодим уже сейчас, чтобы кривая полностью умещалась в экран
	
	-- ***********************************************
	-- На этом заканчивается та часть метода, которая в будущем будет изменена.
	-- Дальше работаем с этими данными ровно так, как собираемся с ними работать в "чистовом" варианте скрипта
	-- Т.е. без захардкодивания чего бы то ни было, с легко читаемым, понимаемым, структруированным кодом
	-- ***********************************************
	
	-- Для начала полученные данные можно проверить на корректность, и если они не годятся -- просто выйти из функции, не рисуя if-then на десять страниц
	
	if not channel or channel:CountKeys() < 2 then 
		print ("Not enough keys or not valid channel")
		return 
	end
	
	-- Если все в порядке (если мы не вышли из функции, значит все в порядке) -- начинаем собирать данные для построения кривой
	-- Ну или сразу строить кривую с дефолтной кривизной, если вам так больше нравится.
	
	
	local nextPointVector = LM.Vector2:new_local() -- контейнер для закидывания значений x и y
	for i=0, channel:CountKeys()-1 do
		nextPointVector.x = channel:GetKeyWhen(i) * Xfactor -- x-кооордината точки будет равна номеру кадра ключа 
		nextPointVector.y = channel:GetValueByID(i) * Yfactor -- y-координата соответствует значению канала в ключе	
		if i==0 then 
			mesh:AddLonePoint(nextPointVector, 0)
		else 
			mesh:AppendPoint(nextPointVector, 0)
		end
	end
	
	-- Кривая построена. Теперь задаем кривизну.

	local curve = mesh:Curve(mesh:CountCurves()-1) -- запоминаем кривую, которой будем обращаться. Это только что построенная кривая, т.е. последняя.
	local interp = MOHO.InterpSetting:new_local() -- объект класса InterpSetting для взятия характеристик ключей канала
	local vect = LM.Vector2:new_local() -- двумерный вектор для векторных вычислений
	
	-- Перебираем точки кривой, чтобы задать входящее и исходящее плечо для каждой
	-- Каждому плечу надо задать угол (offset) и длину (weight). Curvature не трогаем.
	
	for i = 0, curve:CountPoints() - 1 do

		if i > 0 then -- вычисляем входящее плечо
			-- входящее плечо точки на кривой соответствует входящему плечу предыдущего ключа в канале			
			channel:GetKeyInterpByID(i - 1, interp) -- берем параметры предыдущего ключа в переменную interp
			if interp.interpMode == MOHO.INTERP_BEZIER then -- если интерполяция Bezier
				local absAngle = interp:BezierInAngle() -- абсолютный угол плеча относительно оси X
				local frameLength = interp:BezierInPercentage() * (channel:GetKeyWhen(i) - channel:GetKeyWhen(i-1)) -- длина плеча в кадрах
				-- находим абсолютную позицию кончика плеча в экранных единицах:
				vect:Set(-frameLength * Xfactor, 0) -- создаем вектор соответствующей длины из нуля налево
				vect:Rotate(absAngle) -- поворачиваем вектор на найденный угол по часовой стрелке
				-- здесь мы получили вектор при условии, что Xfactor == Yfactor. Теперь нам надо деформировать его с учетом того, что это может быть не так
				vect.y = vect.y * Yfactor/Xfactor
				curve:SetControlHandle(i, vect + curve:Point(i).fPos, 0, true, false) -- задаем позицию плеча относительно позиции самой точки
			else
				-- TODO: нужно научиться обрабатывать так же и другие режимы интерполяции: EasyIn, EasyOut, EasyInOut и Linear. 
				-- скорее всего, последняя строка (curve:SetControlHandle) останется общей для всех режимов, включая Bezier,
				-- но значение vect для них надо будет вычислять по-разному
			end
		end
		
		if i < (curve:CountPoints() - 1) then -- вычисляем исходящее плечо
			-- исходящее плечо точки на кривой соответствует исходящему плечу соответствующего ключа в канале
			channel:GetKeyInterpByID(i, interp) -- берем параметры соответствующего ключа в переменную interp
			if interp.interpMode == MOHO.INTERP_BEZIER then -- если интерполяция Bezier
				local absAngle = interp:BezierOutAngle() -- абсолютный угол плеча относительно оси X
				local frameLength = interp:BezierOutPercentage() * (channel:GetKeyWhen(i + 1) - channel:GetKeyWhen(i)) -- длина плеча в кадрах
				-- находим абсолютную позицию кончика плеча в экранных единицах:
				vect:Set(frameLength * Xfactor, 0) -- создаем вектор соответствующей длины из нуля направо
				vect:Rotate(absAngle) -- поворачиваем вектор на найденный угол против часовой стрелки
				-- здесь мы получили вектор при условии, что Xfactor == Yfactor. Теперь нам надо деформировать его с учетом того, что это может быть не так
				vect.y = vect.y * Yfactor/Xfactor
				curve:SetControlHandle(i, vect + curve:Point(i).fPos, 0, false, false) -- задаем позицию плеча относительно позиции самой точки
			else
				-- TODO: нужно научиться обрабатывать так же и другие режимы интерполяции: EasyIn, EasyOut, EasyInOut и Linear.
			end			
		end
		
	end
	
end


